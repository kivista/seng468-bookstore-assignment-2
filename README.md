# SENG468 Bookstore Assignment 2

Assignment 2 for SENG468 2022 class

Question 1:

The Redis Data Structure used for this type of task would be a sorted set

Assignment Progress:
Users can submit orders which will be stored in a redis cache
Users can then click the "orders" button which displays all orders made in the current session

    - This does not currently return in a sorted order by date

    - No button to return to the main page, must navigate manually back to http://localhost:3000/
