const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

const redisClient = require('./redis-client');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


app.get('/:key', async (req, res) => {
  const { key } = req.params;
  const rawData = await redisClient.getAsync(key);
  return res.json(JSON.parse(rawData));
});

app.get('/', function(req, res) {
  return res.sendFile(path.join(__dirname, '/index.html'));
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});

app.post('/index.html', async function(req, res){
  let name = req.body.name;
  let book = req.body.book;
  let date = req.body.date;
  let value = "{\"name\":\"" + name + "\",\"book\":\"" + book + "\",\"date\":\"" + date + "\"}";
  const key = "orders";
  await redisClient.setAsync(key, value);
  return res.sendFile(path.join(__dirname, '/index.html'));
});